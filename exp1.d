// Эксперимент 1: помещение текста из поля ввода в заголовок и надпись при нажатии на кнопку

import core.runtime;
import std.stdio;

import qte5; // Подключим QtE5
 
alias WindowType = QtE.WindowType;
alias normalWindow = WindowType.Window;

// Обработчики событий
extern (C)
{
	void btnClick(Form* mainFormPointer)
	{
		(*mainFormPointer).getText();
	}
}

class Form : QWidget
{
	private
	{
		QVBoxLayout	vblAll; // Общий вертикальный выравниватель
		QHBoxLayout	hblKn; // Горизонтальный выравниватель для кнопок
		QLineEdit lineEdit; // поле ввода
		QLabel label; // Надпись
		QPushButton btn; // Кнопки
		QAction action; // События кнопок
	}

	this(QWidget parent, WindowType fl)
	{
		super(parent, fl);
		resize(300, 100);
		setWindowTitle("Отображение содержимого поля ввода");

		// Горизонтальный и вертикальный выравниватели
		vblAll = new QVBoxLayout(this);
		hblKn  = new QHBoxLayout(this);

		// Надпись
		label = new QLabel(this);
		label.setStyleSheet("color: blue");

		lineEdit = new QLineEdit(this);

		// Кнопки
		btn = new QPushButton("Отобразить", this);

		// События
		action = new QAction(this, &btnClick, aThis); 

		// Связываем нажатие кнопок с обработчиками событий
		connects(btn, "clicked()", action, "Slot()");

		// Кнопки в горизонтальный выравниватель
		hblKn.addWidget(btn);

		// Всё в вертикальный выравниватель
		vblAll.addWidget(label).addWidget(lineEdit).addLayout(hblKn);

		// Вертикальный выравниватель в форму
		setLayout(vblAll);
	}

	// метод получения текста
	void getText()
	{
		auto str = lineEdit.text!string; // получение содержимого поля ввода
		label.setText("<h1>"~str~"</h1>"); // передаем в лейбл
		setWindowTitle(str); // передаем в заголовок окна
	}
}
 
int main(string[] args)
{
	Form mainForm;
	if (1 == LoadQt(dll.QtE5Widgets, true)) return 1;
	QApplication app = new QApplication(&Runtime.cArgs.argc, Runtime.cArgs.argv, 1);

	with (mainForm = new Form(null, normalWindow))
  {
    show;
    saveThis(&mainForm);
  }

	return app.exec();
}