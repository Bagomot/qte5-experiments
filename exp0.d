// Эксперимент 0: меняется цвет текста при нажатии на 2 кнопки

import core.runtime;
import std.stdio;

import qte5; // Подключим QtE5
 
alias WindowType = QtE.WindowType;
alias normalWindow = WindowType.Window;

// Обработчики событий
extern (C)
{
	void btn1Click(Form* mainFormPointer)
	{
		(*mainFormPointer).getColor(0);
	}

	void btn2Click(Form* mainFormPointer)
	{
		(*mainFormPointer).getColor(1);
	}
}

class Form : QWidget
{
	private
	{
		QVBoxLayout	vblAll; // Общий вертикальный выравниватель
		QHBoxLayout	hblKn; // Горизонтальный выравниватель для кнопок
		QLabel label; // Надпись
		QPushButton btn1, btn2; // Кнопки
		QAction action1, action2; // События кнопок
		string s = "А какую выберешь ты?";
	}

	this(QWidget parent, WindowType fl)
	{
		super(parent, fl);
		resize(300, 100);
		setWindowTitle(s);

		// Горизонтальный и вертикальный выравниватели
		vblAll = new QVBoxLayout(this);
		hblKn  = new QHBoxLayout(this);

		// Надпись
		label = new QLabel(this);
		label.setText("<h1>"~s~"</h1");
		label.setStyleSheet("color: blue");

		// Кнопки
		btn1 = new QPushButton("Зеленую", this);
		btn2 = new QPushButton("Красную", this);

		// События
		action1 = new QAction(this, &btn1Click, aThis); 
		action2 = new QAction(this, &btn2Click, aThis); 

		// Связываем нажатие кнопок с обработчиками событий
		connects(btn1, "clicked()", action1, "Slot()");
		connects(btn2, "clicked()", action2, "Slot()");

		// Кнопки в горизонтальный выравниватель
		hblKn.addWidget(btn1).addWidget(btn2);

		// Всё в вертикальный выравниватель
		vblAll.addWidget(label).addLayout(hblKn);

		// Вертикальный выравниватель в форму
		setLayout(vblAll);
	}

	// метод присвоения цвета надписи
	void getColor(int num)
	{
		if(num == 1)
		{
			label.setStyleSheet("color: red");
		}
		else
		{
			label.setStyleSheet("color: green");
		}
	}
}
 
int main(string[] args)
{
	Form mainForm;
	if (1 == LoadQt(dll.QtE5Widgets, true)) return 1;
	QApplication app = new QApplication(&Runtime.cArgs.argc, Runtime.cArgs.argv, 1);

	with (mainForm = new Form(null, normalWindow))
  {
    show;
    saveThis(&mainForm);
  }

	return app.exec();
}